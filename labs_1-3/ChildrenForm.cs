﻿using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace labs_1_3
{
    public partial class ChildrenForm : Form
    {
        private enum Columns
        {
            COLUMN_MAPID = 0,
            COLUMN_NAME,
            COLUMN_MINLEVEL,
            COLUMN_MAXLEVEL
        }

        public ChildrenForm()
        {
            InitializeComponent();
        }

        public int faction
        {
            get
            {
                return faction_id;
            }

            set
            {
                faction_id = value;
                area_table_adapter.FillByID(WDB_dataset.area, faction_id);
            }
        }


        private void ClientFormLoad(object sender, System.EventArgs e)
        {
            area_table_adapter.Fill(WDB_dataset.area);
        }

        private void OnCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            DataGridViewRow my_row = children_data_grid.Rows[e.RowIndex];

            object name = my_row.Cells[(int)Columns.COLUMN_NAME].Value,
                min_level = my_row.Cells[(int)Columns.COLUMN_MINLEVEL].Value,
                max_level = my_row.Cells[(int)Columns.COLUMN_MAXLEVEL].Value,
                map_id = my_row.Cells[(int)Columns.COLUMN_MAPID].Value;

            if (name == null || map_id == null)
                return;

            area_table_adapter.UpdateByMapID
                (
                    name as string,
                    min_level as int?,
                    max_level as int?,
                    (int)map_id
                );
        }

        private bool IsIn(int map_id)
        {
            return (area_table_adapter.CountByID(map_id) > 0);
        }

        private void OnAddChildClicked(object sender, System.EventArgs e)
        {
            NewChildMessageBox input_box = new NewChildMessageBox();
            input_box.ShowDialog();

            if (!input_box.IsOKPressed)
                return;

            if (IsIn(input_box.MapID))
            {
                DialogResult res = MessageBox.Show
                    (
                        @"MapID " + input_box.MapID + " already exists in the database.\n Do you wish to override it?",
                        "Entry conflict",
                        MessageBoxButtons.OKCancel,
                        MessageBoxIcon.Exclamation
                    );

                if (res == DialogResult.OK)
                    area_table_adapter.UpdateByMapID
                        (
                            input_box.AreaName,
                            input_box.MinLevel,
                            input_box.MaxLevel,
                            input_box.MapID
                        );

                return;
            }

            area_table_adapter.Insert
                (
                    input_box.MapID,
                    input_box.AreaName,
                    input_box.MinLevel,
                    input_box.MaxLevel,
                    faction_id
                );

            area_table_adapter.FillByID(WDB_dataset.area, faction_id);
        }

        private void OnRowsRemoved(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Cancel)
                return;

            DataGridViewRow deleted_row = e.Row;
            area_table_adapter.Delete
                (
                    (int)deleted_row.Cells[(int)Columns.COLUMN_MAPID].Value,
                    deleted_row.Cells[(int)Columns.COLUMN_NAME].Value as string,
                    deleted_row.Cells[(int)Columns.COLUMN_MINLEVEL].Value as int?,
                    deleted_row.Cells[(int)Columns.COLUMN_MAXLEVEL].Value as int?,
                    faction_id
                );
        }

        private void OnKeyPress(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    OnAddChildClicked(sender, null);
                    break;
                case Keys.Escape:
                    Close();
                    break;
                default:
                    break;
            }
        }

        private int faction_id;
    }
}
