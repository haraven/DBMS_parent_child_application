﻿namespace labs_1_3
{
    partial class NewChildMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.input_pane = new System.Windows.Forms.TableLayoutPanel();
            this.map_id_label = new System.Windows.Forms.Label();
            this.name_label = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.max_level_input = new System.Windows.Forms.TextBox();
            this.min_level_input = new System.Windows.Forms.TextBox();
            this.name_input = new System.Windows.Forms.TextBox();
            this.map_id_input = new System.Windows.Forms.TextBox();
            this.buttons_pane = new System.Windows.Forms.TableLayoutPanel();
            this.ok_btn = new System.Windows.Forms.Button();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.input_pane.SuspendLayout();
            this.buttons_pane.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.input_pane, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttons_pane, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.41259F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.58741F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(370, 284);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // input_pane
            // 
            this.input_pane.ColumnCount = 1;
            this.input_pane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.input_pane.Controls.Add(this.map_id_label, 0, 0);
            this.input_pane.Controls.Add(this.name_label, 0, 2);
            this.input_pane.Controls.Add(this.label3, 0, 4);
            this.input_pane.Controls.Add(this.label4, 0, 6);
            this.input_pane.Controls.Add(this.max_level_input, 0, 7);
            this.input_pane.Controls.Add(this.min_level_input, 0, 5);
            this.input_pane.Controls.Add(this.name_input, 0, 3);
            this.input_pane.Controls.Add(this.map_id_input, 0, 1);
            this.input_pane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.input_pane.Location = new System.Drawing.Point(3, 3);
            this.input_pane.Name = "input_pane";
            this.input_pane.RowCount = 8;
            this.input_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.input_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.input_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.input_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.input_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.input_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.input_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.input_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.input_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.input_pane.Size = new System.Drawing.Size(364, 242);
            this.input_pane.TabIndex = 1;
            // 
            // map_id_label
            // 
            this.map_id_label.AutoSize = true;
            this.map_id_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.map_id_label.Location = new System.Drawing.Point(3, 0);
            this.map_id_label.Name = "map_id_label";
            this.map_id_label.Size = new System.Drawing.Size(359, 30);
            this.map_id_label.TabIndex = 8;
            this.map_id_label.Text = "MapID:";
            this.map_id_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // name_label
            // 
            this.name_label.AutoSize = true;
            this.name_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_label.Location = new System.Drawing.Point(3, 60);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(359, 30);
            this.name_label.TabIndex = 9;
            this.name_label.Text = "Name:";
            this.name_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(359, 30);
            this.label3.TabIndex = 10;
            this.label3.Text = "MinLevel:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(359, 30);
            this.label4.TabIndex = 11;
            this.label4.Text = "MaxLevel:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // max_level_input
            // 
            this.max_level_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.max_level_input.Location = new System.Drawing.Point(3, 213);
            this.max_level_input.Name = "max_level_input";
            this.max_level_input.Size = new System.Drawing.Size(359, 20);
            this.max_level_input.TabIndex = 4;
            this.max_level_input.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // min_level_input
            // 
            this.min_level_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.min_level_input.Location = new System.Drawing.Point(3, 153);
            this.min_level_input.Name = "min_level_input";
            this.min_level_input.Size = new System.Drawing.Size(359, 20);
            this.min_level_input.TabIndex = 3;
            this.min_level_input.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // name_input
            // 
            this.name_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_input.Location = new System.Drawing.Point(3, 93);
            this.name_input.Name = "name_input";
            this.name_input.Size = new System.Drawing.Size(359, 20);
            this.name_input.TabIndex = 2;
            this.name_input.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // map_id_input
            // 
            this.map_id_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.map_id_input.Location = new System.Drawing.Point(3, 33);
            this.map_id_input.Name = "map_id_input";
            this.map_id_input.Size = new System.Drawing.Size(359, 20);
            this.map_id_input.TabIndex = 1;
            this.map_id_input.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // buttons_pane
            // 
            this.buttons_pane.BackColor = System.Drawing.SystemColors.Control;
            this.buttons_pane.ColumnCount = 2;
            this.buttons_pane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buttons_pane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buttons_pane.Controls.Add(this.ok_btn, 0, 0);
            this.buttons_pane.Controls.Add(this.cancel_btn, 1, 0);
            this.buttons_pane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttons_pane.Location = new System.Drawing.Point(3, 251);
            this.buttons_pane.Name = "buttons_pane";
            this.buttons_pane.RowCount = 1;
            this.buttons_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buttons_pane.Size = new System.Drawing.Size(364, 30);
            this.buttons_pane.TabIndex = 2;
            // 
            // ok_btn
            // 
            this.ok_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ok_btn.Location = new System.Drawing.Point(3, 3);
            this.ok_btn.Name = "ok_btn";
            this.ok_btn.Size = new System.Drawing.Size(176, 24);
            this.ok_btn.TabIndex = 5;
            this.ok_btn.Text = "OK";
            this.ok_btn.UseVisualStyleBackColor = true;
            this.ok_btn.Click += new System.EventHandler(this.OnOKClicked);
            // 
            // cancel_btn
            // 
            this.cancel_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancel_btn.Location = new System.Drawing.Point(185, 3);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(176, 24);
            this.cancel_btn.TabIndex = 6;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.OnCancelClicked);
            // 
            // NewChildMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 284);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "NewChildMessageBox";
            this.Text = "Enter new child\'s data";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.input_pane.ResumeLayout(false);
            this.input_pane.PerformLayout();
            this.buttons_pane.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel input_pane;
        private System.Windows.Forms.TableLayoutPanel buttons_pane;
        private System.Windows.Forms.Button ok_btn;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Label map_id_label;
        private System.Windows.Forms.Label name_label;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox max_level_input;
        private System.Windows.Forms.TextBox min_level_input;
        private System.Windows.Forms.TextBox name_input;
        private System.Windows.Forms.TextBox map_id_input;
    }
}