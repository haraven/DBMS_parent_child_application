﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace labs_1_3
{
    public partial class ParentForm : Form
    {
        public ParentForm()
        {
            InitializeComponent();
            factions_grid.SelectionChanged += new EventHandler(OnSelectionChanged);
        }

        private void ParentFormLoad(object sender, EventArgs e)
        {
            faction_table_adapter.Fill(WDB_data_set.faction);
        }

        private bool IsChildOpen()
        {
            if (child_form == null)
                return false;
            Form child = Application.OpenForms[child_form.Name];
            if (child != null)
                return true;
            return false;
        }

        private void OnShowChildrenClicked(object sender, EventArgs e)
        {
            if (IsChildOpen())
            {
                return;
            }

            child_form = new ChildrenForm();
            DataGridViewRow current_selection = factions_grid.CurrentRow;
            child_form.faction = (int)current_selection.Cells[0].Value;
            child_form.Show();
        }

        private void OnSelectionChanged(object sender, EventArgs e)
        {
            DataGridViewRow current_selection = factions_grid.CurrentRow;
            object faction_id                 = current_selection.Cells[0].Value;

            if (child_form != null)
                child_form.faction = (int)faction_id;
        }

        private void OnKeyPress(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    OnShowChildrenClicked(sender, null);
                    break;
                case Keys.Escape:
                    Close();
                    break;
                default:
                    break;
            }
        }

        private ChildrenForm child_form;
    }
}
