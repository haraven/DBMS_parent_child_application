﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace labs_1_3
{
    public partial class NewChildMessageBox : Form
    {
        public NewChildMessageBox()
        {
            InitializeComponent();
        }

        private void OnCancelClicked(object sender, System.EventArgs e)
        {
            is_ok_pressed = false;
            Close();
        }

        private void OnOKClicked(object sender, System.EventArgs e)
        {
            is_ok_pressed = true;
            List<string> errors = new List<string>();

            bool res = int.TryParse(map_id_input.Text, out map_id);
            if (!res)
                errors.Add("MapID must be an integer");
            else if (map_id < 0)
                errors.Add("MapID cannot be negative");

            if (name_input.Text == "")
                errors.Add("Name cannot be null");
            else
                name = name_input.Text;

            int tmp;
            if (int.TryParse(min_level_input.Text, out tmp))
                if (tmp < 0)
                    errors.Add("MinLevel cannot be negative");
                else
                    min_level = tmp;
            else if (min_level_input.Text.Equals(""))
                min_level = null;
            else
                errors.Add("MinLevel must be either an integer or a null string");

            if (int.TryParse(max_level_input.Text, out tmp))
                if (tmp < 0)
                    errors.Add("MaxLevel cannot be negative");
                else if (min_level > tmp)
                    errors.Add("MaxLevel cannot be less than MinLevel");
                else
                    max_level = tmp;
            else if (max_level_input.Text.Equals(""))
                max_level = null;
            else
                errors.Add("MaxLevel must be either an integer or a null string");

            if (errors.Count > 0)
            {
                string error_msg = "";
                errors.ForEach(error => error_msg += "- " + error + '\n');
                MessageBox.Show(error_msg, errors.Count + " error(s) occurred", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Close();
        }

        private void OnKeyPress(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    OnOKClicked(sender, null);
                    break;
                case Keys.Escape:
                    OnCancelClicked(sender, null);
                    break;
                default:
                    break;
            }
        }

        public int MapID
        {
            get
            {
                return map_id;
            }
        }

        public string AreaName
        {
            get
            {
                return name;
            }
        }

        public int? MinLevel
        {
            get
            {
                return min_level;
            }
        }

        public int? MaxLevel
        {
            get
            {
                return max_level;
            }
        }

        public bool IsOKPressed
        {
            get
            {
                return is_ok_pressed;
            }
        }

        private int map_id;
        private string name;
        private int? min_level;
        private int? max_level;
        private bool is_ok_pressed;
    }
}
