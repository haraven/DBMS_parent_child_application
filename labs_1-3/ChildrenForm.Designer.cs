﻿namespace labs_1_3
{
    partial class ChildrenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.main_layout_pane = new System.Windows.Forms.TableLayoutPanel();
            this.main_pane = new System.Windows.Forms.TableLayoutPanel();
            this.children_data_grid = new System.Windows.Forms.DataGridView();
            this.mapIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minLevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxLevelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FactionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.area_binding_source = new System.Windows.Forms.BindingSource(this.components);
            this.WDB_dataset = new labs_1_3.WDB_data_set();
            this.add_child_btn = new System.Windows.Forms.Button();
            this.WDB_data_set_binding_source = new System.Windows.Forms.BindingSource(this.components);
            this.area_table_adapter = new labs_1_3.WDB_data_setTableAdapters.areaTableAdapter();
            this.main_layout_pane.SuspendLayout();
            this.main_pane.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.children_data_grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.area_binding_source)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WDB_dataset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WDB_data_set_binding_source)).BeginInit();
            this.SuspendLayout();
            // 
            // main_layout_pane
            // 
            this.main_layout_pane.ColumnCount = 1;
            this.main_layout_pane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_layout_pane.Controls.Add(this.main_pane, 0, 0);
            this.main_layout_pane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_layout_pane.Location = new System.Drawing.Point(0, 0);
            this.main_layout_pane.Name = "main_layout_pane";
            this.main_layout_pane.RowCount = 1;
            this.main_layout_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_layout_pane.Size = new System.Drawing.Size(548, 351);
            this.main_layout_pane.TabIndex = 0;
            // 
            // main_pane
            // 
            this.main_pane.ColumnCount = 1;
            this.main_pane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_pane.Controls.Add(this.children_data_grid, 0, 0);
            this.main_pane.Controls.Add(this.add_child_btn, 0, 1);
            this.main_pane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_pane.Location = new System.Drawing.Point(3, 3);
            this.main_pane.Name = "main_pane";
            this.main_pane.RowCount = 2;
            this.main_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.85507F));
            this.main_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.14493F));
            this.main_pane.Size = new System.Drawing.Size(542, 345);
            this.main_pane.TabIndex = 0;
            // 
            // children_data_grid
            // 
            this.children_data_grid.AllowUserToAddRows = false;
            this.children_data_grid.AutoGenerateColumns = false;
            this.children_data_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.children_data_grid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.children_data_grid.BackgroundColor = System.Drawing.Color.Brown;
            this.children_data_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.children_data_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mapIDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.minLevelDataGridViewTextBoxColumn,
            this.maxLevelDataGridViewTextBoxColumn,
            this.FactionID});
            this.children_data_grid.DataSource = this.area_binding_source;
            this.children_data_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.children_data_grid.Location = new System.Drawing.Point(3, 3);
            this.children_data_grid.Name = "children_data_grid";
            this.children_data_grid.Size = new System.Drawing.Size(536, 303);
            this.children_data_grid.TabIndex = 1;
            this.children_data_grid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellValueChanged);
            this.children_data_grid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.OnRowsRemoved);
            this.children_data_grid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // mapIDDataGridViewTextBoxColumn
            // 
            this.mapIDDataGridViewTextBoxColumn.DataPropertyName = "MapID";
            this.mapIDDataGridViewTextBoxColumn.HeaderText = "MapID";
            this.mapIDDataGridViewTextBoxColumn.Name = "mapIDDataGridViewTextBoxColumn";
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // minLevelDataGridViewTextBoxColumn
            // 
            this.minLevelDataGridViewTextBoxColumn.DataPropertyName = "MinLevel";
            this.minLevelDataGridViewTextBoxColumn.HeaderText = "MinLevel";
            this.minLevelDataGridViewTextBoxColumn.Name = "minLevelDataGridViewTextBoxColumn";
            // 
            // maxLevelDataGridViewTextBoxColumn
            // 
            this.maxLevelDataGridViewTextBoxColumn.DataPropertyName = "MaxLevel";
            this.maxLevelDataGridViewTextBoxColumn.HeaderText = "MaxLevel";
            this.maxLevelDataGridViewTextBoxColumn.Name = "maxLevelDataGridViewTextBoxColumn";
            // 
            // FactionID
            // 
            this.FactionID.DataPropertyName = "FactionID";
            this.FactionID.HeaderText = "FactionID";
            this.FactionID.Name = "FactionID";
            this.FactionID.Visible = false;
            // 
            // area_binding_source
            // 
            this.area_binding_source.DataMember = "area";
            this.area_binding_source.DataSource = this.WDB_dataset;
            // 
            // WDB_dataset
            // 
            this.WDB_dataset.DataSetName = "WDB_data_set";
            this.WDB_dataset.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // add_child_btn
            // 
            this.add_child_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.add_child_btn.Location = new System.Drawing.Point(3, 312);
            this.add_child_btn.Name = "add_child_btn";
            this.add_child_btn.Size = new System.Drawing.Size(536, 30);
            this.add_child_btn.TabIndex = 2;
            this.add_child_btn.Text = "Add child";
            this.add_child_btn.UseVisualStyleBackColor = true;
            this.add_child_btn.Click += new System.EventHandler(this.OnAddChildClicked);
            // 
            // area_table_adapter
            // 
            this.area_table_adapter.ClearBeforeFill = true;
            // 
            // ChildrenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 351);
            this.Controls.Add(this.main_layout_pane);
            this.Name = "ChildrenForm";
            this.Text = "Areas owned by faction";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            this.main_layout_pane.ResumeLayout(false);
            this.main_pane.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.children_data_grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.area_binding_source)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WDB_dataset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WDB_data_set_binding_source)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_layout_pane;
        private System.Windows.Forms.BindingSource WDB_data_set_binding_source;
        private WDB_data_set WDB_dataset;
        private System.Windows.Forms.BindingSource area_binding_source;
        private WDB_data_setTableAdapters.areaTableAdapter area_table_adapter;
        private System.Windows.Forms.TableLayoutPanel main_pane;
        private System.Windows.Forms.DataGridView children_data_grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn mapIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minLevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxLevelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FactionID;
        private System.Windows.Forms.Button add_child_btn;
    }
}