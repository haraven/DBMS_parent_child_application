﻿namespace labs_1_3
{
    partial class ParentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.main_layout_pane = new System.Windows.Forms.TableLayoutPanel();
            this.show_children_btn = new System.Windows.Forms.Button();
            this.factions_grid = new System.Windows.Forms.DataGridView();
            this.entryIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creatureIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.factionBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.WDB_data_set = new labs_1_3.WDB_data_set();
            this.factionBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.faction_binding_source = new System.Windows.Forms.BindingSource(this.components);
            this.factionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.faction_table_adapter = new labs_1_3.WDB_data_setTableAdapters.factionTableAdapter();
            this.main_layout_pane.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.factions_grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.factionBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WDB_data_set)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.factionBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.faction_binding_source)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.factionBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // main_layout_pane
            // 
            this.main_layout_pane.ColumnCount = 1;
            this.main_layout_pane.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_layout_pane.Controls.Add(this.show_children_btn, 0, 1);
            this.main_layout_pane.Controls.Add(this.factions_grid, 0, 0);
            this.main_layout_pane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_layout_pane.Location = new System.Drawing.Point(0, 0);
            this.main_layout_pane.Name = "main_layout_pane";
            this.main_layout_pane.RowCount = 2;
            this.main_layout_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.75156F));
            this.main_layout_pane.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.24845F));
            this.main_layout_pane.Size = new System.Drawing.Size(509, 322);
            this.main_layout_pane.TabIndex = 0;
            // 
            // show_children_btn
            // 
            this.show_children_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.show_children_btn.Location = new System.Drawing.Point(3, 291);
            this.show_children_btn.Name = "show_children_btn";
            this.show_children_btn.Size = new System.Drawing.Size(503, 28);
            this.show_children_btn.TabIndex = 2;
            this.show_children_btn.Text = "Show entry\'s children";
            this.show_children_btn.UseVisualStyleBackColor = true;
            this.show_children_btn.Click += new System.EventHandler(this.OnShowChildrenClicked);
            // 
            // factions_grid
            // 
            this.factions_grid.AllowUserToAddRows = false;
            this.factions_grid.AllowUserToDeleteRows = false;
            this.factions_grid.AutoGenerateColumns = false;
            this.factions_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.factions_grid.BackgroundColor = System.Drawing.Color.Brown;
            this.factions_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.factions_grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.entryIDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.creatureIDDataGridViewTextBoxColumn});
            this.factions_grid.DataSource = this.factionBindingSource2;
            this.factions_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.factions_grid.Location = new System.Drawing.Point(3, 3);
            this.factions_grid.MultiSelect = false;
            this.factions_grid.Name = "factions_grid";
            this.factions_grid.ReadOnly = true;
            this.factions_grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.factions_grid.Size = new System.Drawing.Size(503, 282);
            this.factions_grid.TabIndex = 1;
            this.factions_grid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            // 
            // entryIDDataGridViewTextBoxColumn
            // 
            this.entryIDDataGridViewTextBoxColumn.DataPropertyName = "EntryID";
            this.entryIDDataGridViewTextBoxColumn.HeaderText = "EntryID";
            this.entryIDDataGridViewTextBoxColumn.Name = "entryIDDataGridViewTextBoxColumn";
            this.entryIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.entryIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // creatureIDDataGridViewTextBoxColumn
            // 
            this.creatureIDDataGridViewTextBoxColumn.DataPropertyName = "CreatureID";
            this.creatureIDDataGridViewTextBoxColumn.HeaderText = "CreatureID";
            this.creatureIDDataGridViewTextBoxColumn.Name = "creatureIDDataGridViewTextBoxColumn";
            this.creatureIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.creatureIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // factionBindingSource2
            // 
            this.factionBindingSource2.DataMember = "faction";
            this.factionBindingSource2.DataSource = this.WDB_data_set;
            // 
            // wDB_data_set1
            // 
            this.WDB_data_set.DataSetName = "WDB_data_set";
            this.WDB_data_set.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // factionBindingSource1
            // 
            this.factionBindingSource1.DataMember = "faction";
            this.factionBindingSource1.DataSource = this.WDB_data_set;
            // 
            // factionTableAdapter1
            // 
            this.faction_table_adapter.ClearBeforeFill = true;
            // 
            // ParentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 322);
            this.Controls.Add(this.main_layout_pane);
            this.Name = "ParentForm";
            this.Text = "Factions";
            this.Load += new System.EventHandler(this.ParentFormLoad);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyPress);
            this.main_layout_pane.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.factions_grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.factionBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WDB_data_set)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.factionBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.faction_binding_source)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.factionBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_layout_pane;
        private System.Windows.Forms.BindingSource factionBindingSource;
        private WDB_data_set WDB_data_set;
        private System.Windows.Forms.BindingSource faction_binding_source;
        private WDB_data_setTableAdapters.factionTableAdapter faction_table_adapter;
        private System.Windows.Forms.BindingSource factionBindingSource1;
        private System.Windows.Forms.DataGridView factions_grid;
        private System.Windows.Forms.BindingSource factionBindingSource2;
        private System.Windows.Forms.DataGridViewTextBoxColumn entryIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn creatureIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button show_children_btn;
    }
}

